# Copyright 2015 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=dgud tag=${PNV} ]

SUMMARY="Erlang bindings for SDL"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-erlang/rebar
    build+run:
        dev-lang/erlang[>=14.02]
        media-libs/SDL:0[>=1.2.5]
        x11-dri/glu
        x11-dri/mesa
"

src_prepare() {
    default

    # fix file permissions
    edo find "${WORK}" \
        \( -name "*.[eh]rl" -or -name "*.[ch]" -or -name "*.html" \) \
        -exec chmod 0644 {} \;

    # replace version string to avoid git invocation
    edo sed -e "s/git/\"${PV}\"/" -i src/sdl.app.src
}

src_compile() {
    edo rebar compile -v
}

src_install() {
    ROOT_DIR=/usr/$(exhost --target)/lib/erlang/lib/${PNV}/

    insinto ${ROOT_DIR}/ebin
    doins ebin/sdl.app
    doins ebin/sdl*.beam

    insinto ${ROOT_DIR}/include
    doins include/sdl*.hrl

    insinto ${ROOT_DIR}/priv
    doins priv/sdl_driver.so

    insinto ${ROOT_DIR}/src
    doins src/*.hrl
}

